import "./App.css";

import { Scatter } from "react-chartjs-2";
import { useState,  useEffect } from "react";
import Chart from 'chart.js';
import "chartjs-plugin-dragdata";
import { ChartDataLabels } from 'chartjs-plugin-datalabels';
import { Button } from "@material-ui/core";

const WARMUP_PADDING = 30
const MIN_RAST_WIDTH = 10
const DEFAULT_RAST_COUNT = 2
const MAX_TIME = 350
const DEFAULT_TEMP = 50

const gen_init_data = (ct, max_time) => [...Array(DEFAULT_RAST_COUNT * ct + 2).keys()].map(x => {
  const elem_width = (max_time - (ct + 1) * WARMUP_PADDING) / ct
  const elem_idx = Math.floor(x / 2)
  if (x === 0)
    return { x: 0, y: 0 }
  if (x % 2 === 0)
    return { x: WARMUP_PADDING * elem_idx + elem_width * elem_idx, y: DEFAULT_TEMP }
  return { x: WARMUP_PADDING * (elem_idx + 1) + elem_width * elem_idx, y: DEFAULT_TEMP }
})


function App() {
  const [time, setTime] = useState(150)
  const [max_time, setMaxTime] = useState(MAX_TIME)
  const [data, setData] = useState(gen_init_data(DEFAULT_RAST_COUNT, max_time))
  const [dispData, setDispData] = useState({})

  const updateMaxTime = w => {
    setMaxTime(Math.ceil(w))//Math.ceil(w / 5) * 5)
  }

  const addRast = (curr_dat) => {
    const dataCop = JSON.parse(JSON.stringify(curr_dat))
    console.log(JSON.parse(JSON.stringify(curr_dat)))
    const curr_x = dataCop[dataCop.length - 1].x
    const curr_y = dataCop[dataCop.length - 1].y
    dataCop.splice(dataCop.length - 1, 1, { x: curr_x, y: DEFAULT_TEMP }, { x: curr_x + 2 * WARMUP_PADDING, y: DEFAULT_TEMP }, { x: curr_x + 3 * WARMUP_PADDING, y: curr_y })
    updateMaxTime(dataCop[dataCop.length - 1].x)
    setData(dataCop)
  }

  const remRast = (curr_dat) => {
    if (getRastCount() < 1) return
    let dataCop = JSON.parse(JSON.stringify(curr_dat))
    dataCop.splice(dataCop.length - 2, 2)
    updateMaxTime(dataCop[dataCop.length - 1].x)
    setData(dataCop)
  }

  const getRastCount = () => data.length / 2 - 1

  useEffect(() => {
    setDispData({
      datasets: [
        {
          data: data,
          borderColor: "#9B9B9B",
          borderWidth: 1,
          pointRadius: 10,
          pointHoverRadius: 10,
          pointBackgroundColor: "#609ACF",
          pointBorderWidth: 0,
          spanGaps: false,
          showLine: true,
          tension: 0,
          order: 2,
          label: 'temp',
          datalabels: {
            labels: {
              zeit: {
                align: 'start',
                offset: 30,
                borderRadius: 4,
                color: 'white',
                font: {
                  weight: 'bold'
                },
                formatter: function(value, context){
                  var index = context.dataIndex;
                  if (index===0) return null
                  if (context.dataIndex % 2===0) return Math.round(value.x-context.dataset.data[index-1].x)+' min'
                  return null
                  },
                padding: 6,
              },
              temperatur: {
                anchor: 'center',
                align: '180',
                offset: 20,
                borderRadius: 4,
                color: 'white',
                font: {
                  weight: 'bold'
                },
                formatter: function(value, context){
                  var index = context.dataIndex;
                  if (index===0) return null
                  if (context.dataIndex % 2===1) return Math.round(value.y)+'°C'
                  return null
                  },
                padding: 6,
              }
            }/*
            align: function(context) {
              return context.dataIndex % 2 ? 'right' : 'left';
            },
            anchor: function(context) {
              return context.dataIndex % 2 ? 'end' : 'start';
            },
            offset: 30,
            backgroundColor: function(context) {
              return context.dataIndex % 2 ?
                context.dataset.borderColor :
                'rgba(255, 255, 255, 0.8)';
            },
            borderRadius: 4,
            color: 'white',
            font: {
              weight: 'bold'
            },
            formatter: function(value, context){
              var index = context.dataIndex;
              if (index===0) return null
              return context.dataIndex % 2 ? Math.round(value.y) : Math.round(value.x-context.dataset.data[index-1].x)
  	          },
            padding: 6,*/
          }
        },
        {
          data: [{ x: time, y: 0 }, { x: time, y: 100 }],
          borderColor: "#e31b43",
          borderWidth: 1,
          pointRadius: 10,
          pointHoverRadius: 10,
          pointBackgroundColor: "#e31b43",
          pointBorderWidth: 0,
          spanGaps: false,
          showLine: true,
          order: 1,
          label: 'time',
          tension: 0,
          datalabels: {
            labels: {title: null},
            align: 'start',
            anchor: 'start'
          }
        }
      ],
    })
  }, [data, time])


  const options = {
    tooltips: { enabled: true },
    plugins: {
      datalabels: {
        backgroundColor: function (context) {
          return context.dataset.backgroundColor;
        },
        borderRadius: 4,
        color: 'white',
        font: {
          weight: 'bold'
        },
        formatter: Math.round,
        padding: 6
      }
    },
    scales: {
      xAxes: [
        {
          scaleLabel: {
            display: true,
            labelString: "Time [min]",
            fontSize: 14,
          },
          gridLines: { display: false, color: "grey" },
          ticks: {
            fontSize: 14,
            display: true,
            min: 0,
            max: max_time,
            scaleSteps: 60,
            scaleStartValue: -60,
            maxTicksLimit: 4,
            fontColor: "#9B9B9B",
            padding: 10,
            callback: (point) => (point < 0 ? "" : point),
          },
        },
      ],
      yAxes: [
        {
          scaleLabel: {
            display: true,
            labelString: "Temperature [°C]",
            fontSize: 14,
          },
          ticks: {
            display: true,
            min: 0,
            max: 100,
            scaleSteps: 50,
            scaleStartValue: -50,
            maxTicksLimit: 4,
            fontColor: "#9B9B9B",
            padding: 60,
            callback: (point) => (point < -1 ? "" : point),
          },
          gridLines: {
            display: false,
            offsetGridLines: true,
            color: "3C3C3C",
            tickMarkLength: 4,
          },
        },
      ],
    },
    legend: {
      display: false,
    },
    dragData: true,
    dragX: true,

    onDragStart: function (e, element) {
      // only move elements from first dataset
      if (element._index === 0) return false
      if (element._datasetIndex !== 0) return false
    },

    onDrag: function (e, datasetIndex, index, value) {
      if (index % 2 === 0 && index != 0) {
        value.y = data[index - 1].y
        let x_cut = Math.max(data[index - 1].x + MIN_RAST_WIDTH, value.x)
        if (index < data.length - 2)
          x_cut = Math.min(x_cut, Math.max(data[index + 2].x - MIN_RAST_WIDTH - WARMUP_PADDING))
        data[index].x = x_cut
        data[index].y = value.y
        const offset = x_cut + WARMUP_PADDING - data[index + 1].x 
        for(let i = index + 1; i < data.length; i++){
          data[i].x = data[i].x + offset
        }
        updateMaxTime(data[data.length-1].x)
      }
      else {
        value.x = data[index - 1].x + WARMUP_PADDING
        data[index] = value

        if (index !== data.length - 1) {
          data[index + 1] = { x: data[index + 1].x, y: value.y }
        }
      }
      setData(data)
    },
    onDragEnd: function (e, datasetIndex, index, value) {
      // console.log(datasetIndex, index, value, 'END');
    },
  };

  return (
    <div className="App">
      <header className="App-header">
        <div style={{ width: "80%" }}>
          <Scatter data={dispData} options={options} plugins={ChartDataLabels} />
          <Button variant="contained" color="primary" onClick={()=>addRast(data)}>Rast hinzufügen</Button>
          <Button variant="contained" color="primary" onClick={()=>remRast(data)} disabled={!getRastCount(data)}>Rast entfernen</Button>
        </div>
      </header>
    </div>
  );
}

export default App;
