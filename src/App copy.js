import logo from "./logo.svg";
import "./App.css";

import { Line, Scatter, Area } from "react-chartjs-2";
import { useState, d3, useEffect } from "react";
import Chart from 'chart.js';
import "chartjs-plugin-dragdata";
// import ChartDataLabels from 'chartjs-plugin-datalabels';
import { Button } from "@material-ui/core";

function App() {
  const [RastCounter, setRastCounter] = useState(2)
  const [data, setData] = useState([...Array(2 * RastCounter + 2).keys()].map(x => ({ x: x * 360 / (2 * RastCounter + 1), y: x === 0 ? 0 : 50 })))
  const [prev_data, setPrev_data] = useState([...Array(2 * RastCounter + 2).keys()].map(x => ({ x: x * 360 / (2 * RastCounter + 1), y: x === 0 ? 0 : 50 })))
  const [dispData, setDispData] = useState({})

  useEffect(() => {
    setPrev_data(data)
    setDispData({
      datasets: [
        {
          data: data,
          borderColor: "9B9B9B",
          borderWidth: 1,
          pointRadius: 10,
          pointHoverRadius: 10,
          pointBackgroundColor: "#609ACF",
          pointBorderWidth: 0,
          spanGaps: false,
          showLine: true,
          tension: 0,
          datalabels: {
            align: 'start',
            anchor: 'start'
          }
        },
      ],
    })
  }, [data])

  
  // var RastState = [...Array(2 * RastCounter + 2).keys()].map(x => ({ x: x * 360 / (2 * RastCounter + 1), y: x === 0 ? 0 : 50 }))

  //const [data, setData] = useState(JSON.parse(JSON.stringify(init_ds)))

  //const [RastState, setRastState] = useState([...Array(2*RastCounter+2).keys()].map(x=>({x:x*360/(2*RastCounter+1), y:x===0?0:50})))
  // const [RastState_previous, setRastState_previous] = useState(JSON.parse(JSON.stringify(RastState)))

  //Chart.plugins.register(ChartDataLabels);
  var labels = [];

  for (var i = 0; i < RastCounter; ++i) {
    labels.push('' + i);
  }



  const options = {
    tooltips: { enabled: true },
    plugins: {
      datalabels: {
        backgroundColor: function (context) {
          return context.dataset.backgroundColor;
        },
        borderRadius: 4,
        color: 'white',
        font: {
          weight: 'bold'
        },
        formatter: Math.round,
        padding: 6
      }
    },
    scales: {
      xAxes: [
        {
          scaleLabel: {
            display: true,
            labelString: "Time [min]",
            fontSize: 14,
          },
          gridLines: { display: false, color: "grey" },
          ticks: {
            fontSize: 14,
            display: true,
            min: 0,
            max: 360,
            scaleSteps: 60,
            scaleStartValue: -60,
            // maxTicksLimit: 4,
            fontColor: "#9B9B9B",
            padding: 10,
            callback: (point) => (point < 0 ? "" : point),
          },
        },
      ],
      yAxes: [
        {
          scaleLabel: {
            display: true,
            labelString: "Temperature [°C]",
            fontSize: 14,
          },
          ticks: {
            display: true,
            min: 0,
            max: 100,
            scaleSteps: 50,
            scaleStartValue: -50,
            maxTicksLimit: 4,
            fontColor: "#9B9B9B",
            padding: 30,
            callback: (point) => (point < -1 ? "" : point),
          },
          gridLines: {
            display: false,
            offsetGridLines: true,
            color: "3C3C3C",
            tickMarkLength: 4,
          },
        },
      ],
    },
    legend: {
      display: false,
    },
    dragData: true,
    dragX: true,

    onDragStart: function (e, element) {
      console.log(e, element, 'start');
      if (element._index === 0) return false
    },
    onDrag: function (e, datasetIndex, index, value) {
      console.log(datasetIndex, index, value, 'on');

      if (index != 0){
        const data_copy = JSON.parse(JSON.stringify(prev_data))
        if(index%2 === 0){
          data_copy[index] = { x: value.x, y: data_copy[index].y }
        }
        else {
          data_copy[index] = { x: data_copy[index].x, y: value.y }
          data_copy[index+1] = { x: data_copy[index+1].x, y: value.y }

        }
        setData(data_copy)
      }
      return false
    },
    onDragEnd: function (e, datasetIndex, index, value) {
      console.log(datasetIndex, index, value, 'END');
      if (datasetIndex === 0) {
        if (index === 0) {
          console.log('Here at zero', data.datasets[0].data[0])
          data.datasets[0].data[0] = { x: 0, y: 0 }
          console.log('Here at zero', data.datasets[0].data[0])
        }
      }
    },
  };

  const increaseRast = (e) => {
    setRastCounter(RastCounter + 1)
    // console.log(RastState)
  }
  const decreaseRast = (e) => {
    if (RastCounter > 1) {
      setRastCounter(RastCounter - 1)
    }
  }
  return (
    <div className="App">
      <header className="App-header">
        <div style={{ width: "80%" }}>
          <Scatter data={dispData} options={options} />
          <Button variant="contained" color="primary" onClick={increaseRast}>Rast hinzufügen</Button>
          <Button variant="contained" color="primary" onClick={decreaseRast}>Rast entfernen</Button>
        </div>
      </header>
    </div>
  );
}

export default App;
